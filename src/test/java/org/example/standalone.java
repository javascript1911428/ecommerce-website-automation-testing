package org.example;

import org.example.pageObjects.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class standalone {
    public static void main(String[] args) throws InterruptedException {
        String productName = "ZARA COAT 3";

        WebDriver driver = new ChromeDriver();

        landingPage land = new landingPage(driver);

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        driver.manage().window().maximize();

        land.goTo();

        productCatalouge pl = land.loginApp("injarapuramakrishna55@gmail.com", "Ramki@14533");

        List<WebElement> products = pl.getProductList();

        cartPage cartPage = pl.addProductToCart(productName);

        pl.goToCart();

        Boolean match = cartPage.verifyProductsDisplay(productName);

        Assert.assertTrue(match);

        checkoutPage checkoutPage = cartPage.goToCheckout();

        checkoutPage.selectCountry("india");

        confirmationPage confirmationPage = checkoutPage.submitOrder();

        String confirmMessage = confirmationPage.getConfirmationMessage();

        System.out.println(confirmMessage);

        Assert.assertEquals(confirmMessage, "THANKYOU FOR THE ORDER.");

        driver.close();
    }
}
