package org.example.pageObjects;

import org.example.reusableComponents.abstractComponent;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class landingPage extends abstractComponent {
    WebDriver driver;
    @FindBy(id = "userEmail")
    WebElement userEmail;
    @FindBy(id = "userPassword")
    WebElement passwordEle;
    @FindBy(id = "login")
    WebElement submit;

    @FindBy(css = "[class*='flyInOut']")
    WebElement errorMessage;

    public landingPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

//    public static void main(String[] args) {
//
//    }

    public String getErrorMessage() {
        waitForWebElementToAppear(errorMessage);
        String error = errorMessage.getText();
        return error;
    }
    public productCatalouge loginApp(String email, String password) {
        userEmail.clear();
        userEmail.sendKeys(email);
        passwordEle.clear();
        passwordEle.sendKeys(password);
        submit.click();
        return new productCatalouge(driver);
    }

    public void goTo() {
        driver.get("https://rahulshettyacademy.com/client");
    }
}
