package org.example.pageObjects;

import org.example.reusableComponents.abstractComponent;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class checkoutPage extends abstractComponent {
    WebDriver driver;
    @FindBy(css = ".actions a")
    WebElement submit;
    ////input[@placeholder='Select Country']
    @FindBy(xpath = "//input[@placeholder='Select Country']")
    WebElement country;
    @FindBy(xpath = "//button[contains(@class, 'ta-item')][2]")
    WebElement selectCountry;
    By results = By.cssSelector(".ta-results");

    public checkoutPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(this.driver, this);
    }

    public void selectCountry(String countryName) {
//        Actions a = new Actions(driver);
//        a.sendKeys(country, countryName).build().perform();
//        waitForElementToAppear(results);
//        selectCountry.click();
        country.sendKeys(countryName);
//        waitForElementToAppear(results);
        selectCountry.click();
    }

    public confirmationPage submitOrder() {
        submit.click();
        return new confirmationPage(driver);
    }
}
