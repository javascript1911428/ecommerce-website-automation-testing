package org.example.pageObjects;

import org.example.reusableComponents.abstractComponent;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class productCatalouge extends abstractComponent {
    WebDriver driver;
    @FindBy(css = ".mb-3")
    List<WebElement> products;
    By productsBy = By.cssSelector(".mb-3");
    By toastMessage = By.cssSelector("#toast-container");
    By disappearMeassage = By.cssSelector(".ng-animating");
    public productCatalouge(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public static void main(String[] args) {

    }

    public List<WebElement> getProductList() {
        waitForElementToAppear(productsBy);
        return products;
    }

    public WebElement getProductByName(String productName) {
        WebElement prod = getProductList().stream().filter(product -> product.findElement(By.cssSelector("b")).getText().equals(productName)).findFirst().orElse(null);

        //prod.findElement(By.cssSelector(".card-body button:last-of-type")).click();

        return prod;
    }

    public cartPage addProductToCart(String productName) throws InterruptedException {
        WebElement prod = getProductByName(productName);
        prod.findElement(By.cssSelector(".card-body button:last-of-type")).click();
        waitForElementToAppear(toastMessage);
        Thread.sleep(1000);
        //waitForElementToDisappear(disappearMeassage);
        return new cartPage(driver);
    }

}
