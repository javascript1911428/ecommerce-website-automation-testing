package org.example.pageObjects;

import org.example.reusableComponents.abstractComponent;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class cartPage extends abstractComponent {
    WebDriver driver;
    @FindBy(css = ".totalRow button")
    WebElement checkoutEle;
    @FindBy(css = ".cartSection h3")
    private List<WebElement> cartProducts;

    public cartPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public static void main(String[] args) {

    }

    public Boolean verifyProductsDisplay(String productName) {
        Boolean match = cartProducts.stream().anyMatch(cartPro -> cartPro.getText().equalsIgnoreCase(productName));
        return match;
    }

    public checkoutPage goToCheckout() {
        checkoutEle.click();
        return new checkoutPage(driver);
    }
}
