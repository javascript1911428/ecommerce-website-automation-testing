package org.example;

import org.example.pageObjects.cartPage;
import org.example.pageObjects.checkoutPage;
import org.example.pageObjects.confirmationPage;
import org.example.pageObjects.productCatalouge;
import org.example.testComponents.baseTest;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.List;

public class errorValidationsTest extends baseTest {

    @Test
    public void loginValidation() throws IOException, InterruptedException {
        String productName = "ZARA COAT 3";

        productCatalouge pl = land.loginApp("injarapuramakrishna55@gmail.com", "Ramki@1433");

        Assert.assertTrue(land.getErrorMessage().equalsIgnoreCase("Incorrect email or password."));

        System.out.println(driver.getWindowHandle());

        Thread.sleep(3000);
    }


    @Test
    public  void productErrorValidation() throws InterruptedException {
        String productName = "ZARA COAT 3";

        productCatalouge pl = land.loginApp("ramkiramki196@gmail.com", "Ramki#14533");

        System.out.println(driver.getWindowHandle());

        List<WebElement> products = pl.getProductList();

        cartPage cartPage = pl.addProductToCart(productName);

        pl.goToCart();

        Boolean match = cartPage.verifyProductsDisplay(productName);

        Assert.assertTrue(match);


    }

}
