package org.example.data;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;

public class DataReader {
    public List<HashMap<String, String>> getJsonDataToMap() throws IOException {
        try {
            String jsonContent = FileUtils.readFileToString(new File("src/test/java/org/example/data/purchaseOrder.json"), StandardCharsets.UTF_8);

            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(jsonContent, new TypeReference<List<HashMap<String, String>>>() {});
        } catch (IOException e) {
            e.printStackTrace(); // Handle or log the exception as needed
            return null; // Or return an empty list, depending on your error-handling strategy
        }
    }
}
