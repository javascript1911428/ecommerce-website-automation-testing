package org.example;

import org.example.pageObjects.*;
import org.example.testComponents.baseTest;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class submitOrderTest extends baseTest {
    String productName = "ZARA COAT 3";

    @Test (dataProvider = "getData", groups = {"purchase"})

    public void submitOrder(HashMap<String, String > dataFields) throws IOException, InterruptedException {

        productCatalouge pl = land.loginApp(dataFields.get("email"), dataFields.get("password"));

        List<WebElement> products = pl.getProductList();

        cartPage cartPage = pl.addProductToCart(dataFields.get("productName"));

        pl.goToCart();

        Boolean match = cartPage.verifyProductsDisplay(dataFields.get("productName"));

        Assert.assertTrue(match);

        checkoutPage checkoutPage = cartPage.goToCheckout();

        checkoutPage.selectCountry("india");

        confirmationPage confirmationPage = checkoutPage.submitOrder();

        String confirmMessage = confirmationPage.getConfirmationMessage();

        System.out.println(confirmMessage);

        Assert.assertEquals(confirmMessage, "THANKYOU FOR THE ORDER.");

    }
    @Test (dependsOnMethods = {"submitOrder"})

    public void orderHistory() {
        productCatalouge productCatalouge = land.loginApp("injarapuramakrishna55@gmail.com","Ramki@14533");
        ordersPage ordersPage = productCatalouge.goToOrders();
        Assert.assertTrue(ordersPage.verifyOrderDisplay(productName));
    }

    @DataProvider
    public Object[][] getData() throws IOException {
        List<HashMap<String, String>> data =  getJsonDataToMap("src/test/java/org/example/data/purchaseOrder.json");
        return new Object[][]  {{data.get(0)},{data.get(1)}};
    }
}
